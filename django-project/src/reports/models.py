from django.db import models
from profiles.models import Profile
from django.urls import reverse

# Create your models here.


class Report(models.Model):
    name = models.CharField(max_length=120, null=True)
    image = models.ImageField(upload_to='reports', blank=True, null=True)
    remarks = models.TextField(null=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('reports:detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('-created',)
