from django.shortcuts import render, get_object_or_404
from profiles.models import Profile
from django.http import JsonResponse
from .utils import get_report_image
from .models import Report
from django.views.generic import ListView, DetailView, TemplateView
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from sales.models import Position, Sale
from sales.models import CSV
from django.utils.dateparse import parse_date
from products.models import Product
from customers.models import Customer
import csv
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


class ReportListView(LoginRequiredMixin, ListView):
    model = Report
    template_name = 'reports/main.html'


class ReportDetailView(LoginRequiredMixin, DetailView):
    model = Report
    template_name = 'reports/detail.html'


class UploadTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'reports/from_file.html'


@login_required
def csv_upload_view(request):
    print('file is being sent  ')
    if request.method == 'POST':

        csv_file_name = request.FILES.get('file').name
        csv_file = request.FILES.get('file')
        obj, created = CSV.objects.get_or_create(file_name=csv_file_name)
        print(csv_file)
        print(type(csv_file_name))
        print(type(csv_file))
        print(csv_file_name)
        if created:
            obj.csv_file = csv_file
            obj.save()
        # csv_file = request.FILES.get('file').name
        # print(csv_file)
        # obj = CSV.objects.get_or_create(file_name=csv_file)
        # print(obj)
        # print(2)
            with open(obj.csv_file.path, 'r') as f:
                print(2)
                reader = csv.reader(f)
                print(2)
                reader.__next__()
                print(2)

                for row in reader:
                    print(row, type(row))
                    data = "".join(row)
                    data = data.split(";")
                    data.pop()
                    transaction_id = data[0]
                    product = data[2]
                    quantity = int(data[3])
                    customer = data[4]
                    date = parse_date(data[5])

                    try:
                        product_obj = Product.objects.get(name__iexact=product)
                    except Product.DoesNotExist:
                        product_obj = None

                    if product_obj is not None:
                        customer_obj, _ = Customer.objects.get_or_create(
                            name=customer)
                        salesman_obj = Profile.objects.get(user=request.user)
                        position_obj = Position.objects.create(
                            product=product_obj, quantity=quantity, created=date)
                        sale_obj = Sale.objects.get_or_create(
                            transaction_id=transaction_id, customer=customer_obj, salesman=salesman_obj, created=date)

                        sale_obj.positions.add(position_obj)
                        sale_obj.positions.add(position_obj)
                        sale_obj.save()
                return JsonResponse({'ex': False})
        else:
            return JsonResponse({'ex': True})
    return HttpResponse()


@login_required
def create_report_view(request):
    if request.is_ajax():
        name = request.POST.get('name')
        remarks = request.POST.get('remarks')
        image = request.POST.get('image')
        img = get_report_image(image)
        author = Profile.objects.get(user=request.user)
        # author = request.user.profile
        # print(author.__dict__)
        print(request.user.profile)
        print(Report)
        Report.objects.create(name=name, remarks=remarks,
                              image=img, author=author)
        return JsonResponse({'msg': 'send'})
    return JsonResponse({})


def render_pdf_view(request, pk):
    template_path = 'reports/pdf.html'
    object = get_object_or_404(Report, pk=pk)
    context = {'obj': object}

    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')
    # to download
    # response['Content-Disposition'] = 'attachment; filename="report.pdf"'
    # to view
    response['Content-Disposition'] = 'filename = "report.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
        html, dest=response,)
    # if error then show some funy view
    if pisa_status.err:
        return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response
